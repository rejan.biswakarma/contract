import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Contract } from '../model/contract';

@Injectable({
  providedIn: 'root',
})
export class RestService {
  constructor(private http: HttpClient) {}

  // D:\Ing-project\contratcs\src\assets\data>json-server --watch .\db.json

  url: string = 'http://localhost:3000/contracts';
  names = { id: ' Dynamic student name : rejan', namess: 'karan' };

  viewContract(id) {
    return this.http.get<Contract>('http://localhost:3000/contracts/' + id);
  }

  updateContract(id, name) {
    const url = 'http://localhost:3000/contracts/' + id;

    return this.http.put<Contract>(url, name);
  }

  getStudentDetails() {
    document.getElementById('names').innerText = this.names.id; //name
    var myImg = <HTMLInputElement>document.getElementById('image'); //signature
    myImg.src = '../../assets/image/ing.png';
  }
}
