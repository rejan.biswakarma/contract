import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RestService } from '../service/rest.service';

@Component({
  selector: 'app-view-contract',
  templateUrl: './view-contract.component.html',
  styleUrls: ['./view-contract.component.css'],
})
export class ViewContractComponent implements OnInit {
  private _http: HttpClient;
  public name: string = 'Rejan';

  // names = { id: 'rejan', namess: 'karan' };
  constructor(private rest: RestService, private httpClient: HttpClient) {
    this._http = httpClient;
  }

  // contract: any = (data as any).default;

  ngOnInit() {
    this.rest.viewContract(1).subscribe((data) => {
      document.getElementById('content').innerHTML = data.name;
      this.rest.getStudentDetails();
    });
  }
}
