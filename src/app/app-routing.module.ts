import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EditContractComponent } from './edit-contract/edit-contract.component';
import { ViewContractComponent } from './view-contract/view-contract.component';

const routes: Routes = [
  { path: 'view-contract', component: ViewContractComponent },
  { path: 'edit-contract', component: EditContractComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
