import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { RestService } from '../service/rest.service';
import { Contract } from '../model/contract';

@Component({
  selector: 'app-edit-contract',
  templateUrl: './edit-contract.component.html',
  styleUrls: ['./edit-contract.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class EditContractComponent implements OnInit {
  contractDetails: Contract;
  constructor(private rest: RestService) {}

  ngOnInit() {
    this.rest.viewContract(1).subscribe((data) => {
      this.contractDetails = data;
    });
  }

  updateContract(con) {
    console.log(con);
    // const app = document.getElementById('content').innerHTML;
    let data = { id: 1, name: con };
    this.rest.updateContract(1, data).subscribe((data) => {
      console.log(data);
    });
  }

  update() {
    this.rest.getStudentDetails();
  }
}
